# controlmybot

We first need Node.JS, it includes npm with it, (we need it too for the next step.). [https://nodejs.org/en/download/](https://nodejs.org/en/download/)

You now need to install tmi.js with npm, do this command : 

`npm install tmi.js`

Now you can run backup.bat that will reload the bot if it's down.

You can also run the bot in the terminal, do this command :  

`node bot.js`

(Remember to be in the bot file using the cd command)
