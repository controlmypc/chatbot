// Tell pterodactyl that "yo, container do be running bro"
console.info("done")

// Log Copyright notice
console.info("----------------------------------------------------");
console.info("           CONTROLMYBOT                             ");
console.info("           https://cmpc.live                        ");
console.info("           © 2020-2021 controlmypc                  ");
console.info("           by CMPC Developers                       ");
console.info("----------------------------------------------------");
const tmi = require('tmi.js');

// Configuration
const config = require('./config.json');
const opts = {
  identity: {
    username: config.auth.username,
    password: config.auth.token
  },
  channels: config.channels
};

const client = new tmi.client(opts);

client.on('message', onMessageHandler);
client.on('connected', onConnectedHandler);
client.connect();

// Dictionary with all commands - commandName : output
var commands = {
  '!rules': `Rules are found in the twitch panels or on ${config.domain}/rules`,
  'nightbot': `DONT TALK ABOUT HIM HERE OK, HE'S RUDE`,
  'pog': `PogU`,
  '!racc': `RaccAttack`,
  'e': `E`,
  'cheese': `bonk.. cheese StinkyCheese`,
  '!testconn': `hi there dude!`,
  '!web': `${config.domain} >> offical website of controlmypc`,
  '!website': `${config.domain} >> offical website of controlmypc`,
  '!help': `PLEASE READ THE RULES FIRST ${config.domain}/rules before you go here for the commands: ${config.domain}/commands`,
  '!commands': `Commands: ${config.domain}/commands`,
  '!seizure': `If your reading this, max likley ran the command so, he has seizures, so if something might make him have one, he can close it, so pretty much if he is watching dont fucking show the shit. Simple as that.`,
  '!timelapse': `Timelapses are sorted into seasons in the playlists of the youtube channel: ${config.domain}/playlists`,
  '!achievements': `Achievements are found on ${config.domain}/achievements`,
  '!request': `You can request Commands/Achievements here: ${config.domain}/request`,
  'PAIN': `PAIN`,
  '!faq 1': `NO you must follow the rules! Check below the stream to see them!`,
  '!faq 2': `This can have several reasons, since there pretty long, and the message would clog up the chat, check below the stream to see the full list. If you cant quite tell, !modalert will alert the mods.`,
  '!faq 3': `No of course not, this pc is owned by twitch chat.`,
  '!faq 4': `8 core xeon cpu 2.3ghz, 12gb ram, gtx 1080`,
  '!faq 5': `Fill out the following form, and we will look into it! Form: ${config.domain}/mod`,
  '!faq 6': `We brewed together this program, chatbot, website and all the parts of this, you can see them below the stream.`,
  '!faq 69': `Nice.`,
  '!faq 420': `Nice.`,
  '!discord': `${config.domain}/discord`,
  '!rule 1': `NO P*RN. PERIOD.`,
  '!rule 2': `Do not shutdown the stream, script or computer.`,
  '!rule 3': `You are allowed to download stuff, but no virus or malware.`,
  '!rule 4': `Do not stream any copyrighted content (netflix, movies) However, music is fine.(until we get DMCA'ed)`,
  '!rule 5': `Ask other people in chat before using the quit/close tab command.`,
  '!abq': `Ask other people in chat before using the quit/close tab command (this will not lead to a permaban if its a first time offense like the other rules, however it will likley lead to a timeout.)`,
  '!rule 6': `NO remote access software including but not limited to zoom, teamviewer, VNC, and ssh`,
  '!rule 7': `NO cryptocurrency mining.`,
  '!veto': `Mods have the right to veto any action on the computer if they feel the action can cause harm or may lead to a possible twitch ban.`,
  '!rule 8': `Dont mess around in core system settings, like gpedit or task scheduler.`,
  '!rule 9': `Don't try to get access to forbidden tools or programs.`,
  '!rule 10': `Most important of all, use common sense and dont be a dick. (that means following rules on other places you go to on the internet (like discord servers) too)`,
  '!rule 69': `Nice.`,
  '!rule 420': `Nice.`,
  '!dbat': `Use common sense and dont be a dick.`,
  '!ams': `${config.domain}/amongusshortcuts`,
  '!tip': `You can help out the project by subscribing, or directly tip us at: ${config.domain}/tip`
};

// Called every time a message comes in
function onMessageHandler(target, context, msg, self) {
  if (self) { return; } // Ignore messages from the bot

  // Remove whitespace from chat message
  const commandNameraw = msg.trim();

  // Make all messages lowercase for those lads that are on mobile or just pressed shift by accident
  const commandName = commandNameraw.toLowerCase();

  // log chat since why not
  console.log("CHAT LOG: " + commandNameraw)

  // Process command
  if (commandName in commands) {
    client.say(target, commands[commandName]);
    console.log(commandName);
  }

  // Special commands
  if (commandName.startsWith('!modalert')) {
    client.say(target, `The mods have been notified. (pls dont use this command to spam, you will be banned.) Note that if the script is not working, this will not work, and its best to go to the !discord and tell one of the mods.`);
    console.log("!modalert");
  }
   
if (commandNameraw === 'RaccAttack') {
    client.say(target, `RaccAttack`);
    console.log("RaccAttack");
  }
    
if (commandNameraw === 'PogChamp') {
    client.say(target, `PogU`);
    console.log("PogChamp");
  }
    
if (commandNameraw === 'PogU') {
    client.say(target, `PogU`);
    console.log("PogU");
  }

  if (commandName.includes('***')) {
    client.say(target, `Dinkus (***) strikes again. He appears when twitch blocks a link, try to split your link like: type google and type .com`);
    console.log("type ***");
  }
}

// Called every time the bot connects to Twitch chat
function onConnectedHandler(addr, port) {
  console.log(`* Connected to ${addr}:${port}`);

  // tell in the channel that we are on since why not
  client.say('controlmypc', `chatbot connected.`);
}
